/*Contents*/

This submission folder contains

1) app-release.apk - The APK file
2) solution_documentation.pdf - Documentation around the Architecture, Navigation flow, Specifications and Installation instructions
3) keystore/commbank.jks - The Keystore file for signing the APK for a release
4) mobile-app-commbank-android - The source code
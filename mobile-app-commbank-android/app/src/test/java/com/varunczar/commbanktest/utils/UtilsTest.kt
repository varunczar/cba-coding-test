package com.varunczar.commbanktest.utils

import android.content.Context
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import java.text.SimpleDateFormat
import java.util.*

@RunWith(RobolectricTestRunner::class)
class UtilsTest {

    @Test
    fun testToday()
    {
        Assert.assertEquals("Today",Utils.getNumberOfDays(Date().formatToViewDateDefaults(), RuntimeEnvironment.application))
    }

    @Test
    fun test10daysAgo()
    {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, -10)
        Assert.assertEquals("10 days ago",Utils.getNumberOfDays(calendar.time.formatToViewDateDefaults(), RuntimeEnvironment.application))
    }

    @Test
    fun testInvalidDateFailure()
    {
        Assert.assertEquals("",Utils.getNumberOfDays("zz/zz/zzzz", RuntimeEnvironment.application))
    }

    fun Date.formatToViewDateDefaults(): String{
        val sdf= SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        return sdf.format(this)
    }
}
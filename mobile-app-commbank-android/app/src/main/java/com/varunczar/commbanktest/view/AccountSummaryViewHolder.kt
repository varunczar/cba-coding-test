package com.varunczar.commbanktest.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.varunczar.commbanktest.R
import com.varunczar.commbanktest.model.Account
import com.varunczar.commbanktest.model.EstimateListener
import com.varunczar.commbanktest.utils.Constants.Companion.BLANK
import com.varunczar.commbanktest.utils.Constants.Companion.locale
import java.text.DecimalFormat

/**
 * This view class displays account summary details
 */

class AccountSummaryViewHolder(v: View, private val mEstimateListener: EstimateListener) : RecyclerView.ViewHolder(v) {

    /* Account Name */
    private val mAccountNameTextView: TextView
    /* Account Number */
    private val mAccountNumberTextView: TextView
    /* Avaliable funds */
    private val mAvailableFunds: TextView
    /* Account Balance */
    private val mAccountBalance: TextView
    /* View Projected Spend Message */
    private val mViewProjectedExpenditure: TextView

    /**
     * This onclick is invoked when the View Projected Spend is clicked, it then calls the listener
     * instance to process the click
     */
    private val viewProjectedExpenditureListener = View.OnClickListener { mEstimateListener.onViewEstimatesClicked() }


    init {
        mAccountNameTextView = v.findViewById(R.id.account_name)
        mAccountNumberTextView = v.findViewById(R.id.account_number)
        mAvailableFunds = v.findViewById(R.id.available_funds)
        mAccountBalance = v.findViewById(R.id.account_balance)
        mViewProjectedExpenditure = v.findViewById(R.id.text_action)
        mViewProjectedExpenditure.text = v.context.getString(R.string.label_view_projected_spend)
        mViewProjectedExpenditure.setOnClickListener(viewProjectedExpenditureListener)
    }

    /**
     * This method sets account data from the information received
     *
     * @param account
     */
    fun setAccountSummaryData(account: Account?) {
        /* Return if no data */
        if (account == null) {
            mAccountNameTextView.setText(BLANK)
            mAccountNumberTextView.setText(BLANK)
            mAvailableFunds.setText(BLANK)
            mAccountBalance.setText(BLANK)
            return
        }
        mAccountNameTextView.text = account.accountName
        mAccountNumberTextView.text = account.accountNumber
        mAvailableFunds.text = DecimalFormat.getCurrencyInstance(locale).format(account.available)
        mAccountBalance.text = DecimalFormat.getCurrencyInstance(locale).format(account.balance)
    }
}
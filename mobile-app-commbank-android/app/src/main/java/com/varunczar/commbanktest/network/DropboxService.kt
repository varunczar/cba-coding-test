package com.varunczar.commbanktest.network

import com.varunczar.commbanktest.model.Master
import retrofit2.Call
import retrofit2.http.GET

interface DropboxService {

    @GET("data.json?dl=1")
    fun getMasterRecord(): Call<Master>

}
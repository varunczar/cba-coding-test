package com.varunczar.commbanktest.network

import com.google.gson.GsonBuilder
import com.varunczar.commbanktest.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * This class creates a singleton retrofit client instance to connect to the dropbox URL in the config file
 */

class DropboxClient {

    companion object {
        var retrofit: Retrofit? = null

        fun getClient(): Retrofit {
            //Create a retrofit instance if not created before
            if (retrofit == null) {
                //Create the gson converter
                val gson = GsonBuilder()
                        .setLenient()
                        .create()

                retrofit = Retrofit.Builder()
                        .baseUrl(BuildConfig.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build()
            }
            //Return the retrofit instance
            return retrofit!!
        }
    }
}
package com.varunczar.commbanktest.model

/**
 * This model class groups transactions by date
 */

class TransactionGroup(var account: Account?, var transactions: Map<String?, List<Transaction>>) {

    val mAccount = account
    val mTransactions = transactions


    override fun toString(): String {
        return "TransactionGroup(account=$account, transactions=$transactions"
    }


}

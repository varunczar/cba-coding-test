package com.varunczar.commbanktest.model

/**
 * Model class for pending transactions; has the same properties of it's parent class Transaction
 */
class Pending : Transaction() {
    @Transient
    override var isPending = true

}

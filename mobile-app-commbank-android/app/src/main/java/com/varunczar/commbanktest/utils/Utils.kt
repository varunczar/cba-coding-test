package com.varunczar.commbanktest.utils

import android.content.Context
import android.support.v7.app.AlertDialog
import android.util.Log
import com.varunczar.commbanktest.R
import com.varunczar.commbanktest.model.Atm
import com.varunczar.commbanktest.utils.Constants.Companion.SPACE
import com.varunczar.commbanktest.utils.Constants.Companion.dateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * This is a utility class used by the application
 */

open class Utils {

    companion object {


        val TAG = "Utils"

        /**
         * This comparator is used to search for an atm in a list of atm instances, based on it's atmId
         */
        var atmComparator: Comparator<Atm> = Comparator { atm1, atm2 -> atm1.id!!.compareTo(atm2.id!!) }

        /**
         * Calculates and returns the number of days between a given date and today
         *
         * @param dateString
         * @return
         */
        fun getNumberOfDays(dateString: String?, context: Context): String {
            try {
                val effectiveDate = dateFormat.parse(dateString)
                val diff = Date().time - effectiveDate.getTime()
                val numberOfDays = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
                if (numberOfDays == 0L) {
                    return context.resources.getString(R.string.label_today)
                } else {
                    return numberOfDays.toString() + SPACE + context.resources.getString(R.string.label_days_ago)
                }
            } catch (e: Exception) {
                Log.e(TAG, "Formatting exception ${e.message}")
            }
            return ""
        }


        /**
         * This method shows an alert to provide feedback to the user
         *
         * @param context
         * @param message to display
         */
        fun showErrorDialog(context: Context, message: String) {
            val alertDialog = AlertDialog.Builder(context).create()
            alertDialog.setTitle(context.getString(R.string.message_error))
            alertDialog.setMessage(message)
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, context.getString(R.string.message_ok)
            ) { dialog, which -> dialog.dismiss() }
            alertDialog.setOnShowListener { alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(context.resources.getColor(R.color.colorPrimaryDark)) }
            alertDialog.show()
        }
    }

}

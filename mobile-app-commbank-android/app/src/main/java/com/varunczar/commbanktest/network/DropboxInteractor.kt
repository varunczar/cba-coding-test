package com.varunczar.commbanktest.network

import com.varunczar.commbanktest.model.Master

/**
 * This interface lets implementing classes to fetch accounts data off of dropbox
 */
interface DropboxInteractor {

    fun getTransactions(onFinishedListener: OnFinishedListener)

    interface OnFinishedListener {
        fun onFinished(master: Master)

        fun onFailure()
    }

}
package com.varunczar.commbanktest.view

import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.varunczar.commbanktest.R
import com.varunczar.commbanktest.model.AtmLocationListener
import com.varunczar.commbanktest.model.Transaction
import com.varunczar.commbanktest.utils.Constants.Companion.BLANK
import com.varunczar.commbanktest.utils.Constants.Companion.locale
import com.varunczar.commbanktest.utils.Utils
import java.text.DecimalFormat

class TransactionGroupViewHolder(v: View,
                                 val mTransactions: Map<String?, List<Transaction>>?,
                                 val mAtmLocationListener: AtmLocationListener) : RecyclerView.ViewHolder(v) {

    private val mContext = v.context
    /* Effective date */
    private val mEffectiveDate: TextView
    /* Days since the effective date */
    private val mDaysAgo: TextView
    /* Layout containing a transaction record */
    private val mTransactionRowLayout: LinearLayout
    private val mLayoutParams: LinearLayout.LayoutParams
    private val mPendingLabel: String

    /**
     * This onclick is invoked when an ATM withdrawal transaction row is clicked, it then calls the listener
     * instance to process the click
     */
    private val transactionRecordClickListener = View.OnClickListener { v ->
        val atmId = v.tag as? String
        mAtmLocationListener.onAtmTransactionClicked(atmId)
    }


    init {
        mEffectiveDate = v.findViewById(R.id.text_effective_date)
        mDaysAgo = v.findViewById(R.id.text_days_ago)
        mTransactionRowLayout = v.findViewById(R.id.layout_transaction_row)
        mLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        mPendingLabel = "<b>" + mContext.getString(R.string.label_pending) + "</b>"
    }

    /**
     * This method sets transaction data from a list of transactions per effective date received
     *
     * @param transactionGroupData
     */
    fun setTransactionGroupData(effectiveDate: String?) {
        /* Set the effective date */
        mEffectiveDate.setText(effectiveDate)

        /* Calculate days since effective date and display */
        mDaysAgo.text = Utils.getNumberOfDays(effectiveDate, mDaysAgo.context)

        mTransactionRowLayout.orientation = LinearLayout.VERTICAL

        /* Clear data before adding transaction rows dynamically */
        mTransactionRowLayout.removeAllViews()

        setData(mTransactions?.get(effectiveDate))

    }

    /**
     * This method sets transaction data per transaction for an effective date
     *
     * @param transactionGroupData
     */
    private fun setData(transactions: List<Transaction>?) {
        for (transaction in transactions.orEmpty()) {
            val inflatedLayout = inflateView(transaction)

            setDescription(transaction, inflatedLayout)
            setAmount(transaction, inflatedLayout)

            val filler = inflatedLayout.findViewById<View>(R.id.filler)
            if (transactions?.indexOf(transaction) == transactions?.size?.minus(1)) {
                filler.visibility = View.GONE
            }
            inflatedLayout.layoutParams = mLayoutParams
            inflatedLayout.requestLayout()
            mTransactionRowLayout.addView(inflatedLayout)
        }
    }

    /**
     * This method sets the description of the transaction
     *
     * @param transaction
     * @param inflatedLayout
     */
    private fun setDescription(transaction: Transaction?, inflatedLayout: View) {
        val descriptionTextView = inflatedLayout.findViewById<TextView>(R.id.text_description)
        /* Set a dash if no description present */
        if (transaction == null || transaction.description == null) {
            descriptionTextView.setText(BLANK)
            return
        }
        /* Prepend the label "PENDING" before the description for a pending transaction */
        descriptionTextView.text = if (transaction.isPending)
            Html.fromHtml(mPendingLabel + " " + transaction.description)
        else
            Html.fromHtml(transaction.description)
    }

    /**
     * This method sets the transaction amount
     *
     * @param transaction
     * @param inflatedLayout
     */
    private fun setAmount(transaction: Transaction, inflatedLayout: View) {
        val amountTextView = inflatedLayout.findViewById<TextView>(R.id.text_amount)
        amountTextView.text = DecimalFormat.getCurrencyInstance(locale).format(transaction.amount)
    }

    /**
     * This method inflates a record layout conditionally, if it a normal transactiona default layout,
     * and for ATM withdrawals, a layout with a location icon
     *
     * @param transaction
     * @return
     */
    private fun inflateView(transaction: Transaction): View {
        val inflater = LayoutInflater.from(mContext)
        var layout = R.layout.transaction_record_layout
        if (transaction.atmId != null) {
            layout = R.layout.transaction_record_atm_layout
        }
        val inflatedLayout = inflater.inflate(layout, null, false)
        inflatedLayout.tag = transaction.atmId
        inflatedLayout.setOnClickListener(transactionRecordClickListener)
        return inflatedLayout
    }

}
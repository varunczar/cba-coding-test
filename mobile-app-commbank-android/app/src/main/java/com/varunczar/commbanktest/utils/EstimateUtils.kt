package com.varunczar.commbanktest.utils

import com.varunczar.commbanktest.model.Transaction
import com.varunczar.commbanktest.utils.Constants.Companion.PROBABILITY_DECREMENT
import com.varunczar.commbanktest.utils.Constants.Companion.PROBABILITY_SEED
import com.varunczar.commbanktest.utils.Constants.Companion.START_INDEX_ESTIMATES
import com.varunczar.commbanktest.utils.Constants.Companion.TOTAL_NUMBER_OF_EXPENSES
import com.varunczar.commbanktest.utils.Constants.Companion.dateFormat
import java.text.ParseException
import java.util.*


/**
 * This utility class processes all data used for determining projected spend over the next two
 * weeks for a user
 */

open class EstimateUtils {

    companion object {


        /**
         * This method takes in a list of transactions grouped by date, determines weekly expenses and
         * calculates an estimate of expenses for the next two weeks
         *
         * @param transactionGroups list of transactions grouped by date
         * @return Expenses from weeks 1 - 6 (5 and 6 being the projected expenses)
         */
        fun calculateEstimates(transactionGroups: Map<String?, List<Transaction>>?): DoubleArray? {
            /* If no data present return null */
            if (transactionGroups == null || transactionGroups.isEmpty()) {
                return null
            }
            /* Create and empty array that holds weekly average expenses for 6 weeks
            * Weeks 1 - 4 (Historical data of the weeks gone by) and 5 - 6 projected expenses*/
            val weeklyAverageExpenditures = DoubleArray(TOTAL_NUMBER_OF_EXPENSES)
            try {

                /* Week of the year */
                var woy = -1
                var sum = 0.0
                var count = 0
                var index = START_INDEX_ESTIMATES - 1

                for (transactionGroup in transactionGroups) {
                    val date = dateFormat.parse(transactionGroup.key)

                    /* This section groups transactions by weeks */
                    if (woy != getWeekOfYear(date)) {

                        if (woy != -1) {
                            weeklyAverageExpenditures[index] = sum / count
                            index--
                            if (index == -1) {
                                break
                            }
                        }

                        woy = getWeekOfYear(date)
                        sum = 0.0
                        count = 0
                    }
                    for (transaction in transactionGroup.value) {
                        /* For every transaction record per week, calculate expenses */
                        if (transaction.amount < 0) {
                            sum += Math.abs(transaction.amount)
                            count++
                        }
                    }

                }
                /* Calculate projected expenses for Week 5 based on data from Weeks 1- 4*/
                calculateNextWeeksEstimate(weeklyAverageExpenditures, START_INDEX_ESTIMATES, 0)
                /* Calculate projected expenses for Week 6 based on data from Weeks 2- 5*/
                calculateNextWeeksEstimate(weeklyAverageExpenditures, START_INDEX_ESTIMATES + 1, 1)


            } catch (exp: ParseException) {
                exp.printStackTrace()
            } finally {
                return weeklyAverageExpenditures
            }
        }

        /**
         * This method calculates estimates weekly expenses for the next week based on data from the
         * previous 4 weeks based on the formula
         * 0.4 * (WE4) + 0.3 * (WE3) + 0.2 * (WE2) + 0.1 * (WE1)
         * where WE(1..4) are weekly average expenses from weeks 1 - 4
         *
         * @param weeklyAverageExpenditures    an array of weekly average expenses
         * @param startIndex                   index for places 5 and 6 in the weekly expenses array
         * @param endIndexForProbabilityFactor decrement end index for probability
         */
        private fun calculateNextWeeksEstimate(weeklyAverageExpenditures: DoubleArray, startIndex: Int, endIndexForProbabilityFactor: Int) {

            var nextWeeksEstimate = 0.0
            /* Starting with a probabilty of 0.4 */
            var estimateProbability = PROBABILITY_SEED

            for (estimateCounter in startIndex downTo endIndexForProbabilityFactor + 1) {
                /* Multiplying the probability factor with the most recent weekly average expense */
                nextWeeksEstimate += estimateProbability * weeklyAverageExpenditures[estimateCounter]
                /* Decrementing the probablity factor by 0.1 till it reaches a final value of 0.1 */
                estimateProbability -= PROBABILITY_DECREMENT
            }
            /* Add the estimate calculates to the array of weekly average estimates */
            weeklyAverageExpenditures[startIndex] = nextWeeksEstimate
        }


        /**
         * This method takes in a date and returns the week of the year it falls in
         *
         * @param date
         * @return week of the year
         */
        internal fun getWeekOfYear(date: Date): Int {
            val calendar = Calendar.getInstance()
            calendar.time = date
            return calendar.get(Calendar.WEEK_OF_YEAR)
        }
    }
}

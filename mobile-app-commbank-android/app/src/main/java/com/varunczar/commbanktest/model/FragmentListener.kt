package com.varunczar.commbanktest.model

/**
 * Callback interface to send data back to the calling activity
 */

interface FragmentListener {

    /* Called when an ATM withdrawal row is clicked */
    fun onAtmTransactionClicked(atms: List<Atm>, atmId: String)

    /* Called when View Estimates is clicked */
    fun onViewExpensesClicked(weeklyAverageExpenditures: DoubleArray)
}

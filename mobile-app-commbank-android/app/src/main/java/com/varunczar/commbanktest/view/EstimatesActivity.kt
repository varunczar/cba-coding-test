package com.varunczar.commbanktest.view

import android.os.Bundle
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.utils.Utils
import com.github.mikephil.charting.utils.ViewPortHandler
import com.varunczar.commbanktest.R
import com.varunczar.commbanktest.utils.Constants.Companion.DECIMAL_FORMAT_DOUBLE
import com.varunczar.commbanktest.utils.Constants.Companion.DECIMAL_FORMAT_NONE
import com.varunczar.commbanktest.utils.Constants.Companion.GRANULARITY
import com.varunczar.commbanktest.utils.Constants.Companion.KEY_ESTIMATES
import com.varunczar.commbanktest.utils.Constants.Companion.LINE_WIDTH
import com.varunczar.commbanktest.utils.Constants.Companion.START_INDEX_ESTIMATES
import com.varunczar.commbanktest.utils.Constants.Companion.START_INDEX_HISTORICAL
import com.varunczar.commbanktest.utils.Constants.Companion.VALUE_TEXT_SIZE
import com.varunczar.commbanktest.utils.Constants.Companion.locale
import kotlinx.android.synthetic.main.activity_estimates.*
import kotlinx.android.synthetic.main.header_layout.*
import java.text.DecimalFormat
import java.util.*

/**
 * This class is responsbile for displaying 2 line charts
 * 1) Projected Spend Over the next 2 weeks
 * 2) Historical Spend
 */
class EstimatesActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_estimates)

        text_title.text = resources.getString(R.string.title_estimate)
        setSupportActionBar(account_details_titlebar)
        setupToolBar(false)

        var mWeeklyAverageExpenditure: DoubleArray? = null
        var entries: MutableList<Entry> = ArrayList()

        val bundle = intent.extras
        if (bundle != null && bundle.containsKey(KEY_ESTIMATES)) {
            mWeeklyAverageExpenditure = bundle.getDoubleArray(KEY_ESTIMATES)
        } else {
            /* Close activity if no data available */
            com.varunczar.commbanktest.utils.Utils.showErrorDialog(this, getString(R.string.message_no_estimate))
            finish()
        }

        /* Construct the chart for projected spend */
        if (mWeeklyAverageExpenditure != null) {
            entries.add(Entry(1f, mWeeklyAverageExpenditure[START_INDEX_ESTIMATES].toFloat()))
            entries.add(Entry(2f, mWeeklyAverageExpenditure[START_INDEX_ESTIMATES + 1].toFloat()))
        }
        setChartData(chart_expected_spending, entries, START_INDEX_ESTIMATES)

        /* Construct the chart for historical spend */
        entries = ArrayList()
        if (mWeeklyAverageExpenditure != null) {
            for (entryIndex in 0 until START_INDEX_ESTIMATES) {
                val entryCounter = entryIndex + 1
                entries.add(Entry(entryCounter.toFloat(), mWeeklyAverageExpenditure[entryIndex].toFloat()))
            }
        }
        setChartData(chart_historical, entries, START_INDEX_HISTORICAL)

    }

    /**
     * This method configures and constructs the chart
     * @param chart
     * @param entries
     * @param startIndex
     */
    private fun setChartData(chart: LineChart, entries: List<Entry>, startIndex: Int) {
        chart.setBackgroundColor(resources.getColor(R.color.colorBackgroundLightGrey))
        chart.extraRightOffset = Utils.convertDpToPixel(resources.getDimension(R.dimen.offset_right))
        chart.extraBottomOffset = Utils.convertDpToPixel(resources.getDimension(R.dimen.offset_bottom))
        chart.isDragEnabled = false
        chart.setDrawMarkers(false)
        chart.disableScroll()
        chart.setTouchEnabled(false)
        val dataSet = LineDataSet(entries, "") // add entries to dataset
        dataSet.lineWidth = LINE_WIDTH
        dataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
        dataSet.color = resources.getColor(R.color.colorAccent)
        dataSet.valueTextColor = resources.getColor(R.color.colorPrimaryDark)
        dataSet.valueTextSize = VALUE_TEXT_SIZE
        dataSet.setCircleColor(resources.getColor(R.color.colorAccent))
        val lineData = LineData(dataSet)
        lineData.setValueFormatter(EstimatesFormatter())
        chart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        chart.xAxis.setDrawGridLines(false)
        chart.xAxis.isGranularityEnabled = true
        chart.xAxis.granularity = GRANULARITY
        chart.xAxis.valueFormatter = MyXAxisValueFormatter(startIndex)
        chart.axisLeft.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
        chart.axisLeft.setDrawGridLines(false)
        chart.axisLeft.valueFormatter = MyYAxisValueFormatter()
        chart.setDrawGridBackground(false)
        chart.data = lineData
        chart.description.isEnabled = false
        chart.axisRight.isEnabled = false
        chart.legend.isEnabled = false
        chart.invalidate()
    }

    /**
     * This formatter class, formats amount labels for the line chart coordinates appending a $ before
     * the amount
     */
    internal inner class EstimatesFormatter : IValueFormatter {

        private val mFormat: DecimalFormat

        init {
            mFormat = DecimalFormat(DECIMAL_FORMAT_DOUBLE)
        }

        override fun getFormattedValue(value: Float, entry: Entry, dataSetIndex: Int, viewPortHandler: ViewPortHandler): String {
            return DecimalFormat.getCurrencyInstance(locale).format(java.lang.Float.parseFloat(mFormat.format(value.toDouble())).toDouble())
        }
    }

    /**
     * This class formats the Y-Axis amount labels appending a $ before the amount
     */
    internal inner class MyYAxisValueFormatter : IAxisValueFormatter {

        private val mFormat: DecimalFormat

        init {
            mFormat = DecimalFormat(DECIMAL_FORMAT_NONE)
        }

        override fun getFormattedValue(value: Float, axis: AxisBase): String {
            return DecimalFormat.getCurrencyInstance(locale).format(java.lang.Float.parseFloat(mFormat.format(value.toDouble())).toDouble())
        }
    }

    /**
     * This class formats the X-Axis appending a Wk before the number of the week
     */
    internal inner class MyXAxisValueFormatter(index: Int) : IAxisValueFormatter {

        private var mIndex = START_INDEX_HISTORICAL

        init {
            mIndex = index
        }

        override fun getFormattedValue(value: Float, axis: AxisBase): String {
            return applicationContext.getString(R.string.label_week) + (value + mIndex).toInt()
        }

    }


}

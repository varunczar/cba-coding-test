package com.varunczar.commbanktest.model

/**
 * Callback interface for when the user wants to view estimates of predicted spend
 */

interface EstimateListener {

    /* Called when View Estimates is clicked */
    fun onViewEstimatesClicked()
}

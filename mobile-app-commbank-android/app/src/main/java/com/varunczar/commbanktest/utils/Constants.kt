package com.varunczar.commbanktest.utils

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * This class houses all constants used across the application
 */

class Constants {

    companion object {

        /* Locale encoding constants for Australia*/
        private val LANGUAGE_AUSTRALIA = "en_AU"
        private val COUNTRY_AUSTRALIA = "AU"

        val SPACE = " "

        /* Keys to pass information from one view to the other */
        val KEY_ATM = "atm"
        val KEY_ESTIMATES = "estimates"

        /* Formatting patterns for the graph */
        val DECIMAL_FORMAT_DOUBLE = "###,###,##0.00"
        val DECIMAL_FORMAT_NONE = "###,###,##0"
        val BLANK = "-"

        /* Zoom level for the map */
        val ZOOM_LEVEL_MIN = 0f
        val ZOOM_LEVEL_MAX = 15f

        /* Standard Date format */
        var dateFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy")

        /* Australia locale */
        val locale: Locale
            get() = Locale(LANGUAGE_AUSTRALIA, COUNTRY_AUSTRALIA)

        /* Graph configuration */
        val START_INDEX_ESTIMATES = 4
        val START_INDEX_HISTORICAL = 0
        val LINE_WIDTH = 2f
        val VALUE_TEXT_SIZE = 12f
        val GRANULARITY = 1f

        /* Estimates configuration */
        val PROBABILITY_SEED = 0.4
        val PROBABILITY_DECREMENT = 0.1
        val TOTAL_NUMBER_OF_EXPENSES = 6

        /* Orientation change parameters */
        val KEY_MASTER = "master"

    }

}

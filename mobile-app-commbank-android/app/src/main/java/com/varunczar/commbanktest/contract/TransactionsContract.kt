package com.varunczar.commbanktest.contract

import android.os.Bundle
import com.varunczar.commbanktest.model.Account
import com.varunczar.commbanktest.model.Atm
import com.varunczar.commbanktest.model.Transaction

/**
 * This is a contract class that houses the presenter and view contracts
 */
interface TransactionsContract {

    interface TransactionsPresenter {

        fun requestDataFromServer()
        fun findATM(atmId: String?)
        fun calculateEstimates()
        fun saveState(bundle: Bundle)
        fun restoreState(bundle: Bundle)
    }

    interface TransactionsView {
        fun setAccountDetails(account: Account?, transactions: Map<String?, List<Transaction>>)
        fun showLoading()
        fun hideLoading()
        fun showErrorMessage()
        fun showATM(atm: Atm?)
        fun showEstimate(estimates: DoubleArray?)

    }
}
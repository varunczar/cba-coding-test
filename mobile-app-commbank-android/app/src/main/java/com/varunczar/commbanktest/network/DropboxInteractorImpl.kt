package com.varunczar.commbanktest.network

import android.util.Log
import com.varunczar.commbanktest.model.Master
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * This class connects to dropbox, fetches the account details json file and returns the parsed response
 */
open class DropboxInteractorImpl : DropboxInteractor {

    val TAG = "DropboxInteractorImpl"

    override fun getTransactions(onFinishedListener: DropboxInteractor.OnFinishedListener) {

        val service = DropboxClient.getClient().create(DropboxService::class.java)
        val call = service.getMasterRecord()

        Log.i(TAG, "Calling on dropbox for accounts data")
        //Enqueue the call to fetch account details off of dropbox
        call.enqueue(object : Callback<Master> {
            override fun onResponse(call: Call<Master>, response: Response<Master>) {
                if (response.body() != null) {
                    //Successful data returned
                    Log.i(TAG, "Successful")
                    onFinishedListener.onFinished(response.body()!!)
                } else {
                    Log.i(TAG, "Empty data returned")
                    onFinishedListener.onFailure()
                }

            }

            override fun onFailure(call: Call<Master>, t: Throwable) {
                Log.i(TAG, "Unable to fetch data from the server")
                onFinishedListener.onFailure()
            }
        })

    }


}
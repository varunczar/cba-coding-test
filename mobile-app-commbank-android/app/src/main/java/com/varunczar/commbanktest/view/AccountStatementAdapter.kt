package com.varunczar.commbanktest.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.varunczar.commbanktest.R
import com.varunczar.commbanktest.model.Account
import com.varunczar.commbanktest.model.AtmLocationListener
import com.varunczar.commbanktest.model.EstimateListener
import com.varunczar.commbanktest.model.Transaction

class AccountStatementAdapter
(var account: Account?, var transactions: Map<String?, List<Transaction>>?,
 atmLocationListener: AtmLocationListener,
 estimateListener: EstimateListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
        AtmLocationListener, EstimateListener {

    /* View types */
    private val ACCOUNT_SUMMARY = 0
    private val TRANSACTION_GROUP = 1

    private var mKeys = ArrayList(transactions?.keys)

    private var mAtmLocationListener = atmLocationListener

    private var mEstimateListener = estimateListener

    /* Returns the size of the dataset */
    override fun getItemCount(): Int {
        /* Required for initial initialisation */
        if (transactions == null) {
            return 0;
        }
        return transactions?.keys?.size!!.plus(1)
    }

    /* Returns the view type based on the instance type in a list of heterogenous objects */
    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return ACCOUNT_SUMMARY
        }
        return TRANSACTION_GROUP
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewHolder: RecyclerView.ViewHolder
        val inflater = LayoutInflater.from(viewGroup.context)

        when (viewType) {
        /* Inflate an account summary view to display account details */
            ACCOUNT_SUMMARY -> {
                var accountSummaryViewHolder = inflater.inflate(R.layout.account_summary_layout, viewGroup, false)
                viewHolder = AccountSummaryViewHolder(accountSummaryViewHolder, this)
            }
        /* Inflate a transaction group row for transaction details grouped by date */
            else -> {
                var transactionViewHolder = inflater.inflate(R.layout.transaction_layout, viewGroup, false)
                viewHolder = TransactionGroupViewHolder(transactionViewHolder, transactions, this)
            }
        }
        return viewHolder
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        when (viewHolder.itemViewType) {
        /* Setting account summary data */
            ACCOUNT_SUMMARY -> {
                val accountSummaryViewHolder = viewHolder as AccountSummaryViewHolder
                accountSummaryViewHolder.setAccountSummaryData(account)
            }
        /* Setting transaction group data */
            else -> {
                var transactionGroupViewHolder = viewHolder as TransactionGroupViewHolder
                transactionGroupViewHolder.setTransactionGroupData(mKeys.get(position - 1))
            }
        }
    }

    /**
     * This method is invoked when an atm withdrawal row is clicked
     *
     * @param atmId
     */
    override fun onAtmTransactionClicked(atmId: String?) {
        mAtmLocationListener.onAtmTransactionClicked(atmId)
    }

    /**
     * This method is invoked when the "View Projected Spend" button is clicked
     */
    override fun onViewEstimatesClicked() {
        mEstimateListener.onViewEstimatesClicked()
    }
}
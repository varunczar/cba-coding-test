package com.varunczar.commbanktest.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.varunczar.commbanktest.R

/**
 * This is a base activity that activities with common functionalities can inherit from
 */
open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Close the screen when the back button is clicked
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * This method sets up the toolbar configuration
     */
    open fun setupToolBar(isMainActivity: Boolean) {
        //Enable and display the back button
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        if (!isMainActivity) supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //Hide the default title
        supportActionBar?.setDisplayShowTitleEnabled(false)

        //Set the back button icon
        supportActionBar?.setIcon(R.drawable.icon_welcome_logo)
    }
}

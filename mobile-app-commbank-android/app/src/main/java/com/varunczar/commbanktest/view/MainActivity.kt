package com.varunczar.commbanktest.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.varunczar.commbanktest.R
import com.varunczar.commbanktest.contract.TransactionsContract
import com.varunczar.commbanktest.model.*
import com.varunczar.commbanktest.presenter.TransactionsContractImpl
import com.varunczar.commbanktest.utils.Constants.Companion.KEY_ATM
import com.varunczar.commbanktest.utils.Constants.Companion.KEY_ESTIMATES
import com.varunczar.commbanktest.utils.Constants.Companion.TOTAL_NUMBER_OF_EXPENSES
import com.varunczar.commbanktest.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.header_layout.*
import java.util.*

class MainActivity : BaseActivity(),
        TransactionsContract.TransactionsView,
        AtmLocationListener, EstimateListener {

    var transactionsContractImpl = TransactionsContractImpl(this)
    lateinit var accountStatementAdapter: AccountStatementAdapter;

    val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(account_details_titlebar)
        Log.i(TAG, "Creating a new activity")
        setupToolBar(true)
        setupList()

        // If we have a saved state then we can restore it now
        if (savedInstanceState != null) {
            Log.i(TAG, "Restoring state")
            transactionsContractImpl.restoreState(savedInstanceState)
        } else {
            Log.i(TAG, "Fetching server data")
            fetchData()
        }
        account_refresh.setOnRefreshListener {
            Log.i(TAG, "Refreshing server data")
            fetchData()
        }

    }

    /**
     * This method calls on the presenter to fetch accounts data
     */
    fun fetchData() {
        transactionsContractImpl.requestDataFromServer()
    }

    /**
     * This method sets up the account data and the transactions list
     */
    private fun setupList() {
        Log.i(TAG, "Setting up list")
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        account_statement.layoutManager = linearLayoutManager
        accountStatementAdapter = AccountStatementAdapter(null, HashMap<String?, List<Transaction>>(), this, this)
        account_statement.adapter = accountStatementAdapter
    }

    /**
     * This overriden method sets the accounts data and calls on the list adapter to render it
     */
    override fun setAccountDetails(account: Account?, transactions: Map<String?, List<Transaction>>) {
        Log.i(TAG, "Setting fetched account details")
        accountStatementAdapter = AccountStatementAdapter(account, transactions, this, this)
        account_statement.adapter = accountStatementAdapter
    }

    /**
     * This overriden method shows the progress bar
     */
    override fun showLoading() {
        Log.i(TAG, "Showing progress bar")
        load_progress.visibility = View.VISIBLE
    }

    /**
     * This overriden method hides the progress bar
     */
    override fun hideLoading() {
        Log.i(TAG, "Hiding progress bar")
        load_progress.visibility = View.GONE
        account_refresh.isRefreshing = false

    }

    /**
     * This overriden method displays an error message dialog
     */
    override fun showErrorMessage() {
        Log.i(TAG, "Showing error message")
        Utils.showErrorDialog(this, resources.getString(R.string.message_unable_to_fetch_data))
    }

    /**
     * This overriden method calls on the presenter to search for an atm in a list of atm instances
     */
    override fun onAtmTransactionClicked(atmId: String?) {
        Log.i(TAG, "Clicked ATM $atmId")
        if (!TextUtils.isEmpty(atmId))
            transactionsContractImpl.findATM(atmId)
    }


    /**
     * This overriden method calls on the atm location activity to display the location of the atm
     */
    override fun showATM(atm: Atm?) {
        if (atm == null) {
            Log.i(TAG, "No ATM found")
            /* Display an error if the ATM is not found */
            Utils.showErrorDialog(this, getString(R.string.message_no_atm))
        } else {
            Log.i(TAG, "Displaying ATM $atm")
            /* Launch a google map showing the location of the ATM */
            val launchMapsIntent = Intent(this, AtmLocationsActivity::class.java)
            launchMapsIntent.putExtra(KEY_ATM, atm)
            startActivity(launchMapsIntent)
        }
    }

    /**
     * This overriden method calls on the presenter to calculate spending predictions
     */
    override fun onViewEstimatesClicked() {
        Log.i(TAG, "View Estimates clicked")
        transactionsContractImpl.calculateEstimates()
    }

    /**
     * This overriden method calls on estimates activity to display the projected and historical expenditures
     */
    override fun showEstimate(weeklyAverageExpenditures: DoubleArray?) {
        Log.i(TAG, "Displaying Estimates")
        /* Launch the screen with graphs showing historical and projected expenses */
        if (weeklyAverageExpenditures != null && weeklyAverageExpenditures.size == TOTAL_NUMBER_OF_EXPENSES) {
            val launchEstimatesIntent = Intent(this, EstimatesActivity::class.java)
            launchEstimatesIntent.putExtra(KEY_ESTIMATES, weeklyAverageExpenditures)
            startActivity(launchEstimatesIntent)
        } else {
            Utils.showErrorDialog(this, getString(R.string.message_no_estimate))
        }/* Display an error if no data is available */
    }

    /**
     * This method saves the state of the app during orientation change
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        // Save the state
        transactionsContractImpl.saveState(outState)
    }
}

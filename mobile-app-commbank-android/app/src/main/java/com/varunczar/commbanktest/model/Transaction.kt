package com.varunczar.commbanktest.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat

open class Transaction : Comparable<Transaction> {

    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("effectiveDate")
    @Expose
    var effectiveDate: String? = null
    @SerializedName("description")
    @Expose
    var description: String? = null
    @SerializedName("amount")
    @Expose
    var amount: Double = 0.toDouble()
    @SerializedName("atmId")
    @Expose
    var atmId: String? = null

    private val dateFormat = SimpleDateFormat("dd/MM/yyyy")

    /**
     * No args constructor for use in serialization
     */
    constructor() {}

    /**
     * @param amount
     * @param id
     * @param description
     * @param effectiveDate
     * @param atmId
     */
    constructor(id: String, effectiveDate: String, description: String, amount: Double, atmId: String) : super() {
        this.id = id
        this.effectiveDate = effectiveDate
        this.description = description
        this.amount = amount
        this.atmId = atmId
    }

    fun withId(id: String): Transaction {
        this.id = id
        return this
    }

    fun withEffectiveDate(effectiveDate: String): Transaction {
        this.effectiveDate = effectiveDate
        return this
    }

    fun withDescription(description: String): Transaction {
        this.description = description
        return this
    }

    fun withAmount(amount: Double): Transaction {
        this.amount = amount
        return this
    }

    fun withAtmId(atmId: String): Transaction {
        this.atmId = atmId
        return this
    }

    override fun compareTo(o: Transaction): Int {
        try {
            val dateOne = dateFormat.parse(effectiveDate)
            val dateTwo = dateFormat.parse(o.effectiveDate)

            return dateTwo.compareTo(dateOne)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return 0
    }

    @Transient
    open var isPending = false

    override fun toString(): String {
        return "Transaction(id=$id, effectiveDate=$effectiveDate, description=$description, amount=$amount, atmId=$atmId, dateFormat=$dateFormat, isPending=$isPending)"
    }


}

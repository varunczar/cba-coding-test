package com.varunczar.commbanktest.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Model class for Account details
 */
class Account() : Parcelable {

    @SerializedName("accountName")
    @Expose
    var accountName: String? = null
    @SerializedName("accountNumber")
    @Expose
    var accountNumber: String? = null
    @SerializedName("available")
    @Expose
    var available: Double = 0.toDouble()
    @SerializedName("balance")
    @Expose
    var balance: Double = 0.toDouble()

    constructor(parcel: Parcel) : this() {
        accountName = parcel.readString()
        accountNumber = parcel.readString()
        available = parcel.readDouble()
        balance = parcel.readDouble()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(accountName)
        parcel.writeString(accountNumber)
        parcel.writeDouble(available)
        parcel.writeDouble(balance)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Account> {
        override fun createFromParcel(parcel: Parcel): Account {
            return Account(parcel)
        }

        override fun newArray(size: Int): Array<Account?> {
            return arrayOfNulls(size)
        }
    }


}

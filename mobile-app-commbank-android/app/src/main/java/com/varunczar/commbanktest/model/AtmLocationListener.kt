package com.varunczar.commbanktest.model

/**
 * Callback interface for clicks on atm withdrawals
 */

interface AtmLocationListener {

    /* Called when an ATM withdrawal row is clicked */
    fun onAtmTransactionClicked(atmId: String?)
}

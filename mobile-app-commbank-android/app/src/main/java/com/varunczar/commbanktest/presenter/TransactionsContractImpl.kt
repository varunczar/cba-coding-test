package com.varunczar.commbanktest.presenter

import android.os.Bundle
import android.util.Log
import com.varunczar.commbanktest.contract.TransactionsContract
import com.varunczar.commbanktest.model.Atm
import com.varunczar.commbanktest.model.Master
import com.varunczar.commbanktest.model.Transaction
import com.varunczar.commbanktest.network.DropboxInteractor
import com.varunczar.commbanktest.network.DropboxInteractorImpl
import com.varunczar.commbanktest.utils.Constants.Companion.KEY_MASTER
import com.varunczar.commbanktest.utils.EstimateUtils
import com.varunczar.commbanktest.utils.Utils.Companion.atmComparator
import java.util.*

/**
 * This class is the presentation layer responsible for bridging the communications= between the view
 * and the model
 */
open class TransactionsContractImpl(transactionsView: TransactionsContract.TransactionsView) :
        TransactionsContract.TransactionsPresenter,
        DropboxInteractor.OnFinishedListener {

    val TAG = "TransactionsContract"
    val mTransactionsView = transactionsView

    lateinit var mMaster: Master
    lateinit var atms: List<Atm>
    lateinit var transactions: Map<String?, List<Transaction>>

    /**
     * This overriden method interacts with the network layer to fetch data from dropbox
     */
    override fun requestDataFromServer() {
        //Call the view to show the progress bar
        mTransactionsView.showLoading()
        val dropboxInteractorImpl = DropboxInteractorImpl()
        //Call the network layer to fetch account info
        dropboxInteractorImpl.getTransactions(this)

    }

    /**
     * This method is called when a successful response is received
     */
    override fun onFinished(master: Master) {
        //Process the response received
        processRecords(master)
    }

    /**
     * This method processes the master record returned  and subsequently calls on the view to display
     * the processed records
     */
    private fun processRecords(master: Master) {
        //Hide the progress bar
        mTransactionsView.hideLoading()
        mMaster = master
        atms = master.atms.orEmpty()
        //Merge the pending and non-pending transactions
        var transactionsCombined: ArrayList<Transaction> = ArrayList()
        transactionsCombined.addAll(master.transactions as Iterable<Transaction>)
        transactionsCombined.addAll(master.pending as Iterable<Transaction>)
        //Group transactions by effective date
        transactions = transactionsCombined.groupBy { it.effectiveDate }
        //Sort transactions by descending order of date
        Collections.sort(transactionsCombined)
        //Call on the view to display account info
        mTransactionsView.setAccountDetails(master.account, transactions)
    }

    /**
     * This method is called when the network calls fail
     */
    override fun onFailure() {
        //Hide the progress bar
        mTransactionsView.hideLoading()
        //Show the error message
        mTransactionsView.showErrorMessage()
    }

    /**
     * This method finds by ID the atm in a list of atm objects
     */
    override fun findATM(atmId: String?) {
        /* Perform a binary search to look up, by atmId, an atm instance in a list of atm objects */
        val index = Collections.binarySearch(atms,
                Atm(atmId.orEmpty()), atmComparator)
        if (index >= 0 && index < atms.size) {
            mTransactionsView.showATM(atms.get(index))
        } else {
            mTransactionsView.showATM(null)
        }
    }

    /**
     * This method calls on the estimate utils to calculate projected expenses and then send the
     * processed response to the view to render into a chart
     */
    override fun calculateEstimates() {
        mTransactionsView.showEstimate(EstimateUtils.calculateEstimates(transactions))
    }

    /**
     * This method saves the state of the master object during orientation change
     */
    override fun saveState(bundle: Bundle) {
        bundle.putParcelable(KEY_MASTER, mMaster)
    }

    /**
     * This method restores from a stored state, the master record
     */
    override fun restoreState(bundle: Bundle) {
        val master = bundle.getParcelable<Master>(KEY_MASTER)
        if (master != null) {
            //Master record found in state
            Log.i(TAG, "Master record found in state")
            mMaster = master
            //Process records
            processRecords(master)
        } else {
            //Master record not found in state, fetching from the server
            Log.i(TAG, "Master record not found in state, fetching from the server")
            requestDataFromServer()
        }
    }


}
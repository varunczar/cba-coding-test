package com.varunczar.commbanktest.model

import android.os.Parcel
import android.os.Parcelable

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Model class for ATM Location details
 */
class Location : Parcelable {

    @SerializedName("lat")
    @Expose
    var lat: Double = 0.toDouble()
    @SerializedName("lng")
    @Expose
    var lng: Double = 0.toDouble()

    /**
     * No args constructor for use in serialization
     */
    constructor() {}

    /**
     * @param lng
     * @param lat
     */
    constructor(lat: Double, lng: Double) : super() {
        this.lat = lat
        this.lng = lng
    }

    fun withLat(lat: Double): Location {
        this.lat = lat
        return this
    }

    fun withLng(lng: Double): Location {
        this.lng = lng
        return this
    }

    constructor(`in`: Parcel) {
        this.lat = `in`.readDouble()
        this.lng = `in`.readDouble()

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(lat)
        parcel.writeDouble(lng)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Location> {
        override fun createFromParcel(parcel: Parcel): Location {
            return Location(parcel)
        }

        override fun newArray(size: Int): Array<Location?> {
            return arrayOfNulls(size)
        }
    }


}

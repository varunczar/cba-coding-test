package com.varunczar.commbanktest.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Master model class that contains all required data
 */
class Master() : Parcelable {

    @SerializedName("account")
    @Expose
    var account: Account? = null
    @SerializedName("transactions")
    @Expose
    var transactions: List<Transaction>? = null
    @SerializedName("pending")
    @Expose
    var pending: List<Pending>? = null
    @SerializedName("atms")
    @Expose
    var atms: List<Atm>? = null

    constructor(parcel: Parcel) : this() {
        account = parcel.readParcelable(Account::class.java.classLoader)
        atms = parcel.createTypedArrayList(Atm)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(account, flags)
        parcel.writeTypedList(atms)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Master> {
        override fun createFromParcel(parcel: Parcel): Master {
            return Master(parcel)
        }

        override fun newArray(size: Int): Array<Master?> {
            return arrayOfNulls(size)
        }
    }


}

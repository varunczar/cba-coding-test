package com.varunczar.commbanktest.model

import android.os.Parcel
import android.os.Parcelable

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Model class for ATM details
 */
class Atm : Parcelable {

    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("address")
    @Expose
    var address: String? = null
    @SerializedName("location")
    @Expose
    var location: Location? = null

    /**
     * No args constructor for use in serialization
     */
    constructor() {}

    /**
     * @param id
     * @param location
     * @param address
     * @param name
     */
    constructor(id: String, name: String, address: String, location: Location) : super() {
        this.id = id
        this.name = name
        this.address = address
        this.location = location
    }

    /**
     * @param id
     */
    constructor(id: String) : super() {
        this.id = id

    }

    fun withId(id: String): Atm {
        this.id = id
        return this
    }

    fun withName(name: String): Atm {
        this.name = name
        return this
    }

    fun withAddress(address: String): Atm {
        this.address = address
        return this
    }

    fun withLocation(location: Location): Atm {
        this.location = location
        return this
    }

    constructor(`in`: Parcel) {
        this.id = `in`.readString()
        this.name = `in`.readString()
        this.address = `in`.readString()
        this.location = `in`.readParcelable(Location::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(address)
        parcel.writeParcelable(location, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Atm> {
        override fun createFromParcel(parcel: Parcel): Atm {
            return Atm(parcel)
        }

        override fun newArray(size: Int): Array<Atm?> {
            return arrayOfNulls(size)
        }
    }

}

package com.varunczar.commbanktest.view

import android.os.Bundle
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.varunczar.commbanktest.R
import com.varunczar.commbanktest.model.Atm
import com.varunczar.commbanktest.utils.Constants.Companion.KEY_ATM
import com.varunczar.commbanktest.utils.Constants.Companion.ZOOM_LEVEL_MAX
import com.varunczar.commbanktest.utils.Constants.Companion.ZOOM_LEVEL_MIN
import kotlinx.android.synthetic.main.header_layout.*

/**
 * This class displays a map showing the ATM location where a particular withdrawal was made
 */
class AtmLocationsActivity : BaseActivity(), OnMapReadyCallback {

    private var mMap: GoogleMap? = null
    private var mAtm: Atm? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_atm_locations)
        text_title.text = resources.getString(R.string.title_find_us)
        setSupportActionBar(account_details_titlebar)

        val bundle = intent.extras
        if (bundle != null && bundle.containsKey(KEY_ATM)) {
            mAtm = bundle.getParcelable(KEY_ATM)
        }

        setupToolBar(false)

        //Load the map fragment
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * This method anipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        if (mAtm != null) {
            //Create a latitude and longitude instance
            val atmLocation = LatLng(mAtm!!.location!!.lat, mAtm!!.location!!.lng)
            //Set the minimum zoom for the map - 0
            mMap!!.setMinZoomPreference(ZOOM_LEVEL_MIN)
            //Add the logo icon as the marker for the atm location
            mMap!!.addMarker(MarkerOptions()
                    .position(atmLocation)
                    .title(mAtm!!.name)
                    .snippet(mAtm!!.address)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_atm_commbank)))
            //Adjust the focus to centre the marker
            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(atmLocation))
            val cameraPosition = CameraPosition.Builder()
                    .target(atmLocation)
                    .zoom(ZOOM_LEVEL_MAX)
                    .build()
            mMap!!.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }
    }


}